package com.dosac.escuelapp.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dosac.escuelapp.entity.Profile;

@Repository("profileJpaRepository")
public interface ProfileJpaRepository extends JpaRepository<Profile, Serializable> {
	public abstract Profile findByName(String name);
}
