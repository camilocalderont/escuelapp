package com.dosac.escuelapp.controller;


import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.dosac.escuelapp.model.User;
import com.dosac.escuelapp.repository.ProfileJpaRepository;
import com.dosac.escuelapp.service.UserService;

@Controller
@RequestMapping("/")
public class IndexController {
	
	private static final Log LOGGER = LogFactory.getLog(IndexController.class);
	
	
	@GetMapping("/")
	public String root() {
		return "redirect:/inicio";
	}
	
	@GetMapping("/inicio")
	public String index(Model model) {
		//int i = 1 / 0;
		model.addAttribute("user", new User());
		return "index";
		//return "redirect:/login";
	}
	
	@Autowired
	@Qualifier("userService")
	private UserService userService;
	
	@Autowired
	@Qualifier("profileJpaRepository")
	private ProfileJpaRepository profileJpaRepository;
	
	
	@GetMapping("/login")
	public String loginForm(Model model) {
		//model.addAttribute("user", user);
		model.addAttribute("user", new User());
		return "login";
	}	
	
	@PostMapping("/loginValidation")
	public ModelAndView loginValidation(@Valid @ModelAttribute("user") User user, BindingResult bindingResult) {
		ModelAndView mav = new ModelAndView();
		if(bindingResult.hasErrors()) {
			mav.setViewName("index");
		}else {
			mav.setViewName("dashboard");
			//mav.setViewName("mainLayout");
			LOGGER.info("METHOD: 'loginValidation ' -- P:"+user);
			mav.addObject("user", user);
						
		}
		return mav;
	}
	
	
	@GetMapping("/listUsers")
	private String listUsers(Model model) {
		model.addAttribute("user", userService.getListUsers());
		return "listUsers";
	}
	
	@GetMapping("/profile")
	public String profile() {
		//int i = 1 / 0;
		//return "index";
		profileJpaRepository.findByName("docente");
		return "redirect:/login";
	}	

}
