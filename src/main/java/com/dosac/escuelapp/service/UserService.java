package com.dosac.escuelapp.service;

import java.util.List;

import com.dosac.escuelapp.model.User;

public interface UserService {

	public abstract List<User> getListUsers();
}
