package com.dosac.escuelapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.dosac.escuelapp.model.User;
import com.dosac.escuelapp.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {
	private static final Log LOGGER = LogFactory.getLog(UserServiceImpl.class);
	@Override
	public List<User> getListUsers() {
		List<User> users = new ArrayList<>();
		users.add(new User("camilo","12345"));
		users.add(new User("juli","987456"));
		users.add(new User("juan","98632"));
		users.add(new User("javi","1qaz2ws"));
		users.add(new User("paloma","09tghjkn"));
		users.add(new User("camilo","khgtygh"));
		LOGGER.info("Lista de Usuarios desde Service"); 
		return users;
	}

}
