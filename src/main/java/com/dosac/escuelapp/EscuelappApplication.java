package com.dosac.escuelapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EscuelappApplication {

	public static void main(String[] args) {
		SpringApplication.run(EscuelappApplication.class, args);
	}
}
