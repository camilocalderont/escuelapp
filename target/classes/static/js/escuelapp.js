
var url_service = '../src/Administracion/Controlador/AparienciaController.php'; 
var sm=576;
var md=768;
var lg=992;
var xl=1200;
$(document).ready(function() {
	/*efecto enlaces*/
	  $(document).on('click', 'a[href^="#"]', function (event) {
		  if($.attr(this, 'href')!="#"){
			    event.preventDefault();
			    $('html, body').animate({
			      scrollTop: $($.attr(this, 'href')).offset().top
			    }, 500);			  
		  }
	  }); 
	
	/*Codigo para Carousel full Screen*/
	var $item = $('.carousel-item'); 
	var $wHeight = $(window).height();
	$item.eq(0).addClass('active');
	$item.height($wHeight); 
	$item.addClass('full-screen');

	$('.carousel img').each(function() {
	  var $src = $(this).attr('src');
	  var $color = $(this).attr('data-color');
	  $(this).parent().css({
	    'background-image' : 'url(' + $src + ')'
	  });
	  $(this).remove();
	});

	$(window).on('resize', function (){
	  $wHeight = $(window).height();
	  $item.height($wHeight);
	});   

    /*Codigo para Menu Responsive*/
	
	ajustarNav();
	 $(window).resize(function() {
		 ajustarNav();
	 });	
    var menuAbierto=false;
    $("#boton_menu_inicio").click(function(e){
        //alert("h");
        if(!menuAbierto){
            $("#contenedorMenu").animate({
                left: '0'
            });
            equisMenu();
            menuAbierto=true;
        }
        else{
            $("#contenedorMenu").animate({
                left: '-100%'
            }); 
            lineasMenu();
            menuAbierto=false;
        }
    });

    $("#menu-top li a").click(function(e){
        if($(window).outerWidth()<=768){            
            $("#contenedorMenu").animate({
                left: '-100%'
            });     
        }
        lineasMenu();
        menuAbierto=false;  
    }); 

    function equisMenu(){
        $("#boton_menu_inicio").animate({
            'border-radius': '50%'
        });
        $('#boton_menu_inicio .linea_inicio:nth-child(1)').animate({  textIndent: -45 }, {
            step: function(now,fx) {
              $(this).css({'-webkit-transform': 'translateY(9px) rotate('+now+'deg)'}); 
            },
            duration:'fast'
        },'linear');
        $('#boton_menu_inicio .linea_inicio:nth-child(2)').animate({  textIndent: 0 }, {
            step: function(now,fx) {
              $(this).css({'opacity': now}); 
            },
            duration:'fast'
        },'linear');        
        $('#boton_menu_inicio .linea_inicio:nth-child(3)').animate({  textIndent: 45 }, {
            step: function(now,fx) {
              $(this).css({'-webkit-transform': 'translateY(-9px) rotate('+now+'deg)'}); 
            },
            duration:'fast'
        },'linear');             
    }
    
    function lineasMenu(){
        $("#boton_menu_inicio").animate({
            'border-radius': '4px'
        });     
        $('#boton_menu_inicio .linea_inicio:nth-child(1)').animate({  textIndent: 0 }, {
            step: function(now,fx) {
              $(this).css({'-webkit-transform': 'translateY(0px) rotate('+now+'deg)'}); 
            },
            duration:'fast'
        },'linear');  
         
        $('#boton_menu_inicio .linea_inicio:nth-child(3)').animate({  textIndent: 0 }, {
            step: function(now,fx) {
              $(this).css({'-webkit-transform': 'translateY(0px) rotate('+now+'deg)'}); 
            },
            duration:'fast'
        },'linear');   
        $('#boton_menu_inicio .linea_inicio:nth-child(2)').animate({  textIndent: 1 }, {
            step: function(now,fx) {
              $(this).css({'opacity': now}); 
            },
            duration:'fast'
        },'linear');         
    }
    
    
    function ajustarNav(){
    	width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
    	if (width < md) {
		    $('#menu-top').addClass('flex-column');
		} 
		else{
			$('#menu-top').removeClass('flex-column');
		}    	
    }



    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
});
